'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const favicon = require('serve-favicon');
const path = require('path');
const schedule = require('node-schedule');
const passport = require('passport');

const appStrategy = require('./config/strategyConfig');
const auth = require('./controllers/auth');
const formidable = require('express-formidable');

const app = express();

global.env = process.env.NODE_ENV || 'development';

// Load Mongoose Models
require('./models/Member');
require('./models/Image');
require('./models/Token');
require('./models/HomepagePost');
require('./models/Event');
require('./models/Link');

// Initialize Passport JWT Strategy=======
passport.use(appStrategy);
passport.serializeUser(function (user, done) {
  done(null, user._id);
});
passport.deserializeUser(function (id, done) {
  Member.findById(id).then(function (user) {
    done(null, user);
  });
});

// Start middleware =====================
app.use(passport.initialize());
app.use(require('./config/sessionStore').config);
app.use(passport.session());

/*app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.set('views', path.resolve(__dirname + '/../public'));
app.use(express.static(path.resolve(__dirname + '/../public')));*/

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Token, content-type, x-parse-application-id, x-parse-rest-api-key, x-parse-session-token');
  next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

/*app.use(formidable({
  encoding: 'utf-8',
  multiples: true,
  keepExtensions: true,
  hash: false
}));*/

let routes = require('./routes');
app.use('/', routes);
// End middleware =====================

// Clear out all the old tokens every 23 hours
schedule.scheduleJob('23 * * *', async function () {
  await auth.deleteOldTokens();
});

module.exports = app;