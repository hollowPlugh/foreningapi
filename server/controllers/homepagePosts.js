'use strict';

const Post = require('../models/HomepagePost');
const moment = require('moment');
const DB_ERR_MSG = 'Internal database error';

exports.create = async function(req, res) {
  //let body = req.fields;
  let body = req.body;
  if(!body || (!body.title && !body.body)){
    return res.status(400).json({message : 'Either title or body required'})
  }
  let post = new Post(body);
  let newpost = await post.save()
    .catch(err => {
      console.log(err);
      return res.status(500).json({message : DB_ERR_MSG});
      });
  return res.status(201).json({post: newpost});
};

exports.getOneById = async function(req, res) {
  //let body = req.fields;
  let body = req.body;
  if(!body && !body.id) {
    return res.status(400).json({message: 'Homepage post ID required.'});
  }
  let post = await Post.findOne({'_id': body.id})
    .catch(err => {
      console.log(err);
      return res.status(500).json({message : DB_ERR_MSG});
    });
  if(!post){
    return res.status(500).json({message : 'No post with given ID'});
  }
  return res.status(200).json({post: post});
};

exports.update = async function(req, res){
  //let body = req.fields;
  let body = req.body;
  if(!body || !body.id || (!body.title && !body.body)){
    return res.status(400).json({message : 'ID and either title or body required'});
  }
  let post = await Post.findOne({'_id': body.id})
    .catch(err => {
      console.log(err);
      return res.status(500).json({message : DB_ERR_MSG});
    });
  if(!post){
    return res.status(500).json({message: 'No such post with ID ' + body.id});
  }
  post.title = body.title || '';
  post.body = body.body || '';
  post.isVisible = body.isVisible || false;
  post.lastUpdated = moment();
  let savedPost = await post.save()
    .catch(err => {
      console.log(err);
      return res.status(500).json({message : DB_ERR_MSG});
    });
  return res.status(200).json({post: savedPost});
};

exports.remove = async function(req, res) {
  //let body = req.fields;
  let body = req.body;
  if(!body || !body.id){
    return res.status(400).json({message: 'ID required'});
  }
  let post = await Post.findOne({'_id': body.id})
    .catch(err => {
      console.log(err);
      return res.status(500).json({message : DB_ERR_MSG});
    });
  if(!post){
    return res.status(500).json({message: 'No post exists with ID ' + body.id});
  }
  await post.remove()
    .catch(err => {
      console.log(err);
      return res.status(500).json({message : DB_ERR_MSG});
    });
  return res.status(200).json({message: 'Post deleted.'});
};

exports.getAll = async function(req, res){
  let posts = await Post.find({})
    .catch(err => {
      console.log(err);
      return res.status(500).json({message : DB_ERR_MSG});
    });
  return res.status(200).json({posts: posts});
};

exports.getAllVisible = async function(req, res){
  let posts = await Post.find({'isVisible': true})
    .catch(err => {
      console.log(err);
      return res.status(500).json({message : DB_ERR_MSG});
    });
  return res.status(200).send({posts: posts});
};