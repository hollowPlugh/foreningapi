'use strict';

const Event = require('../models/Event');
const DB_ERR_MSG = 'Internal database error';
const imageCtlr = require('./image');

// CREATE
exports.create = async function (req, res) {
  let body = req.fields || null;
  if (!body) {
    return res.status(400).end();
  }
  let messages = validateEventRequest(body);
  if (messages.errors) {
    return res.status(400).json(messages);
  } else {
    let event = new Event();
    event = setEventValues(event, body);
    let savedEvent = await event.save()
      .catch(err => {
        console.log(err);
        return res.status(500).json({message: err.message});
      });
    res.status(201).json({event: savedEvent});
  }
};

// FIND ONE BY ID
exports.getOneById = async function (req, res) {
  let body = req.fields;
  if (!body || !body.id) {
    return res.status(400).json({message: 'Doc ID required.'});
  }
  let query = {"_id": body.id};
  let event = await Event.findOne(query)
    .catch(err => {
      console.log(err);
      return res.status(500).json({message: DB_ERR_MSG});
    });
  if (!event) {
    return res.status(400).json({message: 'Event not found.'});
  }
  return res.status(200).json({event: event});
};

// UPDATE
exports.update = async function (req, res) {
  let body = req.fields;
  if (!body || !body.id) {
    return res.status(400).json({message: 'Event ID required.'});
  }
  let query = {"_id": body.id};
  let event = await Event.findOne(query)
    .catch(err => {
      console.log(err);
      return res.status(500).json({message: DB_ERR_MSG});
    });
  if (!event) {
    return res.status(400).json({message: 'No event with given ID'});
  }
  let messages = validateEventRequest(body);
  if (messages.errors) {
    return res.status(400).json(messages);
  } else {
    event = setEventValues(event, body);
    let updated = await event.save()
      .catch(err => {
        console.log(err);
        return res.status(500).json({message: DB_ERR_MSG});
      });
    return res.status(200).json({event: updated});
  }
};

// DELETE
exports.remove = async function (req, res) {
  let body = req.fields;
  if (!body || !body.id) {
    return res.status(400).json({message: 'Doc ID required.'});
  }
  let query = {"_id": body.id};
  await Event.remove(query)
    .catch(err => {
      console.log(err);
      return res.status(500).json({message: DB_ERR_MSG});
    });
  let result = await imageCtlr.handleDeletedEvent(body.id);
  if(result.error){
    return res.status(500).json(result);
  }
  return res.status(200).json(result);
};
// LIST
exports.getAll = async function (req, res) {
  let events = await Event.find({})
    .catch(err => {
      console.log(err);
      return res.status(500).json({message: DB_ERR_MSG});
    });
  return res.status(200).json({events: events});
};

function setEventValues(event, body) {
  event.title = body.title;
  event.description = body.description || '';
  event.location.address1 = body.address1 || '';
  event.location.address2 = body.address2 || '';
  event.location.postcode = body.postcode || '';
  event.location.city = body.city || '';
  let startDate = body.startDate;
  let startTime = body.startTime ? body.startTime : '00:00';
  let newDateTime = constructDateTime(startDate, startTime);
  if (!newDateTime) {
    return res.status(500).json({message: 'Unable to format start dateTime.'});
  }
  event.dateTimeStart = newDateTime;
  if (body.endDate) {
    let endDate = body.endDate;
    let endTime = body.endTime ? body.endTime : '23:59';
    newDateTime = constructDateTime(endDate, endTime);
    if (!newDateTime) {
      return res.status(500).json({message: 'Unable to format end dateTime.'});
    }
    event.dateTimeEnd = newDateTime;
  }
  return event;
}

function constructDateTime(date, time) {
  try {
    return new Date(date + 'T' + time + 'Z');
  } catch (error) {
    return null;
  }
}

function validateEventRequest(body) {
  let message = '';
  if (!body.title) {
    message = 'Title is required. ';
  }
  if (!body.startDate) {
    message = message + 'Start date is required. ';
  }
  if (body.startDate && body.startTime) {
    try {
      new Date(body.startDate + 'T' + body.startTime + ':00Z')
    } catch (error) {
      message = message + 'Start date should be formatted YYYY-MM-DD. Start time should be formatted HH:MM. ' + error.message;
    }
  } else if (body.startDate && !body.startTime) {
    try {
      new Date(body.startDate + 'T00:00:00Z')
    } catch (error) {
      message = message + 'Start date should be formatted YYYY-MM-DD. ' + error.message
    }
  }
  if (!body.endDate && body.endTime) {
    message = message + 'End date is required to set the event end. ';
  }
  if (body.endDate && body.endTime) {
    try {
      new Date(body.endDate + 'T' + body.endTime + '00Z')
    } catch (error) {
      message = message + 'Date should be formatted YYYY-MM-DD. Time should be formatted HH:MM. ' + error.message;
    }
  } else if (body.endDate && !body.endTime) {
    try {
      new Date(body.endDate + 'T00:00:00Z')
    } catch (error) {
      message = message + 'End date should be formatted YYYY-MM-DD. ' + error.message;
    }
  }
  if (message) {
    return {'errors': message}
  } else {
    return {}
  }
}
