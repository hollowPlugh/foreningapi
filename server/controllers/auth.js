'use strict';

const jwt = require('jsonwebtoken');
const Member = require('../models/Member');
const Token = require('../models/Token');
const mailer = require('./mailer');
const utils = require('../config/utils');

const AUTH_ERROR_MSG = 'Authentication error.';
const AUTH_FAILED_MSG = 'Failed to authenticate';



exports.login = function (req, res) {
  return res.render('login');
};

exports.logout = function (req, res) {
  req.logout();
  res.redirect('/login');
};

exports.adminInterface = function (req, res) {
  return res.render('adminIndex');
};

exports.doLogin = async function (req, res) {
    //let fields = req.fields || null;
    let fields = req.body || null;
    if (!fields || !fields.email || !fields.password) {
      return res.status(400).json({message: AUTH_FAILED_MSG});
    }
    let email = fields.email;
    let password = fields.password;
    let member = await Member.findOne({email: email, isAdmin: true})
      .catch(err => {
        console.log(err);
        return res.status(500).json({message: AUTH_ERROR_MSG});
      });
    if (!member) return res.status(401).json({message: AUTH_FAILED_MSG});
    if (!member.validatePassword(password)) return res.status(401).json({message: AUTH_FAILED_MSG});

    let memberId = {id: member._id};
    let token = jwt.sign(memberId, process.env.JWT_SECRET, {expiresIn: '24h'});
    let newTokenPost = new Token();
    newTokenPost.assignedTo = member.email.toLowerCase();
    newTokenPost.token = token;
    await newTokenPost.save()
      .catch(err => {
        console.log(err);
        return res.status(500).json({message: AUTH_ERROR_MSG})
      });
    return res.status(200).json({token: token});
};

exports.deleteOldTokens = async function () {
  let query = {expires: {$lte: new Date()}};
  await Token.remove(query)
    .catch(err => {
      console.log('Could not erase old tokens: ' + err.message);
    });
};

exports.forgotPassword = async function (req, res) {
  //let body = req.fields || null;
  let body = req.body || null;
  if (!body && !body.email) {
    return res.status(400).json({message : 'Email required.'});
  }
  let email = body.email;
  let query = {'email': email, 'isAdmin': true};
  let member = await Member.findOne(query)
    .catch(err => {
      console.log(err);
      return res.status(500).json({message : AUTH_ERROR_MSG});
    });
  if (!member) {
    return res.status(401).json({message : 'No administrator access for user ' + email});
  } else if (member) {
    let newPassword = mailer.sendNewPassword(member);
    if (newPassword) {
      member.hash = Member.encryptPassword(newPassword);
      await member.save()
        .catch(err => {
          console.log(err);
          return res.status(500).json({message: AUTH_ERROR_MSG});
        });
      return res.status(200).json({message : 'Your new password has been sent to ' + email});
    } else {
      return res.status(500).json({message: 'Unable to create and/or send password to admin'});
    }
  }
};